/**
 * Mock layer abstraction.
 *
 * @author Alejandro
 * @since 0.1.0
 */
package com.acl.spring.seed.mock;
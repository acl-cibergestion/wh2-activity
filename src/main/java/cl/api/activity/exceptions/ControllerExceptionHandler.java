package cl.api.activity.exceptions;

import cl.api.activity.error.Error;
import cl.api.activity.error.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpStatusCodeException;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);
    private static final String ERROR_LOG_STRING = "ERROR: {}{}";
    
    @ExceptionHandler(value = CustomException.class)
    public ResponseEntity<Error> handleCustomException(CustomException ex){
        logger.error(ERROR_LOG_STRING, ex.getMessage(), ex);
        var error = new Error();
        error.setIndex(ex.getIndex());
        error.setCode(ex.getErrorCode());
        error.setDetail(ex.getDetail());
        return ResponseEntity.status(ex.getErrorCode().getStatus()).body(error);
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleException(HttpMessageNotReadableException ex){
        logger.error(ERROR_LOG_STRING, ex.getMessage(), ex);
        var code = ErrorCode.BAD_REQUEST;
        var error = new Error();
        error.setIndex(code.getIndex());
        error.setCode(code);
        error.setDetail(code.getMsg());
        return error;
    }

    @ExceptionHandler(value = HttpStatusCodeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleException(HttpStatusCodeException ex) {
        logger.error(ERROR_LOG_STRING, ex.getMessage(), ex);
        var code = ErrorCode.INTERNAL_ERROR;
        var error = new Error();
        error.setIndex(code.getIndex());
        error.setCode(code);
        error.setDetail(code.getMsg());
        return error;
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleException(Exception ex){
        logger.error(ERROR_LOG_STRING, ex.getMessage(), ex);
        ErrorCode code = ErrorCode.INTERNAL_ERROR;
        var error = new Error();
        error.setIndex(code.getIndex());
        error.setCode(code);
        error.setDetail(code.getMsg());
        return error;
    }

}


package cl.api.activity.controller;

import cl.api.activity.exceptions.CustomException;
import cl.api.activity.models.to.AddCommentRequestTo;
import cl.api.activity.models.to.CamundaResponse;
import cl.api.activity.models.to.CommentTo;
import cl.api.activity.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/camunda")
public class CommentController {

    
    @Autowired
    CamundaCoreService commentService;

    @PostMapping("/addComment")
    public CamundaResponse addComment(@RequestBody AddCommentRequestTo request) throws CustomException {
        return commentService.addComment(request);
    }

    @PostMapping("/findComments")
    public ResponseEntity<List<CommentTo>> findCommentsByCarpetaId(@RequestBody Map<String,String> request) throws CustomException {
        return ResponseEntity.status(HttpStatus.OK).body(commentService.findCommentsByCarpetaId(request));
    }

    @PostMapping("/deleteComment")
    public boolean deleteCommentsByCarpetaId(@RequestBody Map<String, String> request) throws CustomException{
        return commentService.deleteComment(request);
    }

    
}

package cl.api.activity.controller;

import cl.api.activity.service.*;
import cl.api.activity.models.to.*;
import cl.api.activity.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/camunda")
public class GetPreguntaController {

    private final Logger logger = LoggerFactory.getLogger(GetPreguntaController.class);

    @Autowired
    CamundaCoreService camundaCoreService;

	@PostMapping("/getPregunta") 
    public Pregunta getPregunta(@RequestBody GetPreguntaRequestTo request){
        
		logger.info("Entrada GetPreguntaController : getPregunta");
		return camundaCoreService.getPregunta( request.getIdExecution() );
    }

}

package cl.api.activity.controller;

import cl.api.activity.service.*;
import cl.api.activity.exceptions.CustomException;
import cl.api.activity.models.to.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/api/camunda")
public class AvanzarController {

    private final Logger logger = LoggerFactory.getLogger(AvanzarController.class);

    @Autowired
    CamundaCoreService camundaCoreService;

    @PostMapping("/avanzar") 
    public CamundaResponse avanzar(@RequestBody AvanzarRequestTo request) throws CustomException {
    	logger.info("Entrada AvanzarController : avanzar");
        return camundaCoreService.avanzar(request.getIdExecution(), request.getPreguntaRespuesta(), request.getUsername());
    }
}

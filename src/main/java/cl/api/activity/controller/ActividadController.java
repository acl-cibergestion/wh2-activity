package cl.api.activity.controller;

import cl.api.activity.exceptions.CustomException;
import cl.api.activity.service.*;
import cl.api.activity.models.Usuario;
import cl.api.activity.models.to.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@RestController
@RequestMapping("/api/camunda")
public class ActividadController {

    private final Logger logger = LoggerFactory.getLogger(ActividadController.class);

    @Autowired
    CamundaCoreService camundaCoreService;
    
    @PostMapping("/cambiarEstado") 
    public CamundaResponse cambioEstadoCarpeta(@RequestBody CambioEstadoCarpetaDTO cambioEstadoCarpetaDTO) throws CustomException {
    	logger.info("Entrada ActividadController : cambioEstadoCarpeta");
    	return camundaCoreService.cambioEstadoCarpeta(cambioEstadoCarpetaDTO);
    }
    
    @PostMapping("/eliminarActividadHistoria") 
    public CamundaResponse eliminarActividadHistoria(@RequestBody EliminarActividadHistoriaRequest request) throws CustomException {
    	logger.info("Entrada ActividadController : eliminarActividadHistoria");
    	return camundaCoreService.eliminarActividadHistoria(request);
    }
        
    @PostMapping("/borrarActividad") 
    public CamundaResponse borrarActividad(@RequestBody BorrarActividadRequestDTO request) throws CustomException {
    	logger.info("Entrada ActividadController : borrar");
    	return camundaCoreService.borrarActividad(request);
    }
    
    @PostMapping("/descontabilizar") 
    public CamundaResponse descontabilizar(@RequestBody DescontabilizarRequestDTO request) throws CustomException {
    	logger.info("Entrada ActividadController : descontabilizar");
    	return  camundaCoreService.descontabilizar(request);
    }
    
    @PostMapping("/rehacerActividad") 
    public CamundaResponse rehacerActividad(@RequestBody RehacerActividadRequestDTO request) throws CustomException {
        
    	logger.info("Entrada ActividadController : rehacerActividad");
		return camundaCoreService.rehacerActividad(request);
    }

    @PostMapping("/rehacerActividad2")
    public CamundaResponse rehacerActividad2(@RequestBody RehacerActividadRequestDTO2 request) throws CustomException {

        logger.info("Entrada ActividadController : rehacerActividad2");
        return camundaCoreService.rehacerActividad2(request);
    }
    
    @PostMapping("/reasignarAndComment") 
    public CamundaResponse reasignarAndComment(@RequestBody ReasignarActividadesRequestDTO request) throws CustomException {
    	logger.info("Entrada ActividadController : reasignarAndComment");
		return camundaCoreService.reasignarAndComment(request);
    }

    @PostMapping("/eliminarOperacion")
    public CamundaResponse eliminarOperacion(@RequestBody DeleteRequestTo deleteRequestTo) throws CustomException {
        logger.info("Entrada ActividadController : eliminarOperacion");
        return camundaCoreService.eliminarOperacion(deleteRequestTo);
    }

    @PostMapping("/reasignar") 
    public CamundaResponse reasignar(@RequestBody ReasignarRequestDTO request) throws CustomException {
    	logger.info("Entrada ActividadController : reasignar");
		return camundaCoreService.reasignar(request);
    }

    @PostMapping("/agregarActividad") 
    public String agregarActividad(@RequestBody AgregarActividadRequest request){
        
    	logger.info("Entrada ActividadController : agregarActividad");
		return camundaCoreService.agregarActividad(request);
    }

    @PostMapping("/agregarActividad2") 
    public CamundaResponse agregarActividad2(@RequestBody AgregarActividad2Request request) throws CustomException {
        
    	logger.info("Entrada ActividadController : agregarActividad2");
		return camundaCoreService.agregarActividad2(request);
    }
    
    @PostMapping("/agregarActividadPorProceso") 
    public String agregarActividadPorProceso(@RequestBody AgregarActividadPorProcesoRequest request){
        
    	logger.info("Entrada ActividadController : agregarActividadPorProceso");
		return camundaCoreService.agregarActividadPorProceso(request);
    }

    @PostMapping("/cargarTareasAsignadas")
    public List<TareasAsignadasResponseDTO> cargarTareasAsignadas(@RequestBody TareasAsignadasRequestDTO request) throws CustomException {

        logger.info("Entrada ActividadController : cargarTareasAsignadas");
        return camundaCoreService.findRunningTaskByUsuario(request);
    }

    @PostMapping("/responsables")
    public List<UsuarioDto> responsables(@RequestBody RequestDTO request) {

        logger.info("Entrada ActividadController : responsables");
        return camundaCoreService.responsables(request);
    }
    
    @PostMapping("/findTareasPrevias")
    public List<TaskDefResponseTo> findTareasPrevias(@RequestBody TareasPreviasRequestDTO request) {

        logger.info("Entrada ActividadController : findTareasPrevias");
        return camundaCoreService.findTareasPrevias(request);
    }
    
    @PostMapping("/findUsuariosTareasPendientes")
    public List<Usuario> findUsuariosTareasPendientes(@RequestBody RolesOrigenAsigMasivaDTO request) {

        logger.info("Entrada ActividadController : findUsuariosTareasPendientes");
        return camundaCoreService.findUsuariosTareasPendientes(request);
    }
    
    @PostMapping("/reasignacionMasiva")
    public CamundaResponse reasignacionMasiva(@RequestBody ReasignarActividadesRequestDTO request) throws CustomException {

        logger.info("Entrada ActividadController : reasignacionMasiva");
        return camundaCoreService.reasignarAndComment(request);
    }
}

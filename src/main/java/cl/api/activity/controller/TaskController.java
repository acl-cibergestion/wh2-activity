package cl.api.activity.controller;

import cl.api.activity.exceptions.CustomException;
import cl.api.activity.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/camunda")
public class TaskController {

    @Autowired
    TaskService taskService;

    @PostMapping("/findTasks")
    public Map<String, Long> findTasks(@RequestBody(required = false) String qparams) throws CustomException{
        return taskService.findTasks(qparams);
    }

    @PostMapping("/findLastTaskOperacion")
    public Map<Long, Long> lastHistoryRestoInOperacion(@RequestBody(required = false) String qparams) throws CustomException{
        return taskService.lastHistoryRestoInOperacion(qparams);
    }

    @GetMapping("/findTaskByYear/{year}/{regionId}")
    public Boolean findTaskByYear(@PathVariable Integer year, @PathVariable Long regionId) throws CustomException{
        return taskService.findTaskByYear(year, regionId);
    }
    
}

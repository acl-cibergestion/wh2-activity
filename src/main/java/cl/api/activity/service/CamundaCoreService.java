package cl.api.activity.service;

import cl.api.activity.error.ErrorCode;
import cl.api.activity.exceptions.CustomException;
import cl.api.activity.models.*;
import cl.api.activity.models.to.*;
import cl.api.activity.repositories.*;
import cl.api.activity.repositories.impl.CamundaCoreRepositorio;
import cl.api.activity.util.Constant;
import cl.api.activity.util.enumerador.EstadoCarpetaEnum;
import cl.api.activity.util.enumerador.EstadoContabilizacionEnum;
import cl.api.activity.util.enumerador.EtActividadDetalleEnum;
import cl.api.activity.util.enumerador.RolEnum;
import cl.api.activity.util.enumerador.TipoOperacionEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static cl.api.activity.util.Constant.SUCCESSFULLY;


@Service
public class CamundaCoreService {

    private final Logger logger = LoggerFactory.getLogger(CamundaCoreService.class);

    @Autowired
    private CamundaCoreRepositorio camundaCoreRepositorio;
    @Autowired
    private CommentJpaRepository commentJpaRepository;
    @Autowired
    private HistoryTaskRepository historyTaskRepository;
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;
    @Autowired
    private CarpetaRepositorio carpetaRepositorio;
    @Autowired
    private EstadoCarpetaRepositorio estadoCarpetaRepositorio;
    @Autowired
    private RunningExecutionRepository runningExecutionRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private RunningTaskRepository runningTaskRepository;
    @Autowired
    private FormalizacionRepositorio formalizacionRepositorio;
    @Autowired
    private FoContabilizacionRepositorio foContabilizacionRepositorio;
    @Autowired
    private TaskDefinitionRepositorio taskDefinitionRepositorio;
    @Autowired
    private CustomTaskProcessTreeRepository customTaskProcessTreeRepository;
    @Autowired
    private CustomTaskDefinitionRepository customTaskDefinitionRepository;
    @Autowired
    private UsuarioRolRepositorio usuarioRolRepositorio;
    @Autowired
    private RunningTaskSearchRepository runningTaskSearchRepository;
    
    private static final String ESTADO_ELIMINACION = "D";
    private static final String RESPUESTA_FALLIDA = "fallido";
    private static final String CODE_RESPUESTA_EXITOSA = "CAMUNDA_RESPUESTA_EXITOSA";
    private static final String FIND_SIGNED_FOLDER = "/caParticipante/carpetaFirmada/";
    private static final String FIND_ASSIGNED_ROLE = "/user/rolAsignadoUsuario/";
    private static final String DELETE_OPERATION = "Se elimino la operacion con exito";
    private static final Object OBSERVACION = ". Observación: ";

    @Value("${wh2.url.participantes}")
    String urlParticipantes;

    @Value("${wh2.url.perfilamiento}")
    String urlPerfilamiento;

    public Pregunta getPregunta(String idExecution) {

        logger.info("Entrada: CamundaCoreService.getPregunta");
        return camundaCoreRepositorio.getPregunta(idExecution);
    }


    public CamundaResponse avanzar(List<String> idExecution, String preguntaRespuesta, String usuarioEjecutorName) throws CustomException {

        logger.info("Entrada: CamundaCoreService.avanzar");
        logger.info("Entrada: idExecution: {}", idExecution);
        logger.info("Entrada: preguntaRespuesta: {}", preguntaRespuesta);
        logger.info("Entrada: usuarioEjecutorName: {}", usuarioEjecutorName);

        Optional<Usuario> usuarioOpt = usuarioRepositorio.findByNombreUsuario(usuarioEjecutorName);

        var respuestasC = new StringBuilder();
        var respuestasI = new StringBuilder();

        if (usuarioOpt.isEmpty()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
        try {

            var usuario = usuarioOpt.get();
            for (String idExec : idExecution) {
                Object respuesta = camundaCoreRepositorio.avanzar(idExec, preguntaRespuesta, usuario.getId());
                if (Objects.nonNull(respuesta)) {
                    respuestasI.append("Error al avanzar actividad ").append(idExec).append(",");
                } else {
                    respuestasC.append("Actividad avanzada ").append(idExec).append(",");
                }
            }
        } catch (Exception e) {
            logger.error("Error en camunda avanzar: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, respuestasC.toString(), respuestasI.toString());
    }

    public CamundaResponse addComment(AddCommentRequestTo request) throws CustomException {

        boolean response;
        Optional<Usuario> usuarioOpt = usuarioRepositorio.findByNombreUsuario(request.getUsername());
        if (usuarioOpt.isEmpty()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
        
        try {
            var usuario = usuarioOpt.get();
            request.setUsuarioId(usuario.getId().toString());
            request.setComment(request.getComment().replace("á", "&aacute;").replace("é", "&eacute;").replace("í", "&iacute;")
            									   .replace("ó", "&oacute;").replace("ú", "&uacute;"));
            request.setIdOperacion(request.getCarpetaId());
            response = camundaCoreRepositorio.addComment(request);
        } catch (Exception e) {
            logger.error("Error:  {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (!response) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se guardó el comentario exitosamente");

    }

    private CommentTo getCommentTo(HistoryComment comment) {
        var commentTo = new CommentTo();
        var fullMessage = new String(comment.getFullMessage() == null ? "".getBytes() : comment.getFullMessage());
        if (comment.getMessage().length() > 150) {
            commentTo.setMessage(comment.getMessage().substring(0, 150));
        } else {
            commentTo.setMessage(comment.getMessage());
        }
        commentTo.setMessage(comment.getMessage().replace("&aacute;","á").replace("&eacute;","é").replace("&iacute;","í")
        		.replace("&oacute;","ó").replace("&uacute;","ú"));
        commentTo.setFullMessage(fullMessage);
        return commentTo;
    }

    private void setFolderToComment(CommentTo commentTo, Carpeta carpeta) {
        
    	var folderId = Objects.nonNull(carpeta.getId()) ? "[Op.".concat(carpeta.getId().toString()).concat("]")
                    : null;
        var folder = Objects.nonNull(carpeta.getFlujo()) ? carpeta.getFlujo().getDescripcion()
                    : folderId;
        commentTo.setCarpeta(folder);
    }

    public List<CommentTo> findCommentsByCarpetaId(Map<String, String> request) throws CustomException {
        
    	String carpetaId = request.getOrDefault("carpetaId", null);
        if (!StringUtils.hasText(carpetaId)) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        List<HistoryComment> comments = commentJpaRepository.findByProcInstIdOrderByIdDesc(carpetaId);
        List<CommentTo> response = new ArrayList<>();
        if (!comments.isEmpty()) {
            for (HistoryComment comment : comments) {

                var commentTo = getCommentTo(comment);
                //Setear Comment : carpeta
                setFolderToComment(commentTo, comment.getCarpeta());

                //Setear Comment : los demas valores
                commentTo.setId(comment.getId());
                commentTo.setUser(getNombreUsuario(comment.getUsuario()));
                commentTo.setTime(comment.getTime() == null ? "" : dateFormat.format(comment.getTime()));
                commentTo.setUsername(Objects.isNull(comment.getUsuario()) ? "" : comment.getUsuario().getNombreUsuario());
                if(Objects.nonNull(comment.getHistoryTask())) {
                	commentTo.setTask(comment.getHistoryTask() .getName());
                }
                response.add(commentTo);
            }
        }
        return response;
    }


    public boolean deleteComment(Map<String, String> map) throws CustomException {

        var response = false;

        try {
            String commentId = map.getOrDefault("commentId", null);
            if (StringUtils.hasText(commentId)) {
                Optional<HistoryComment> comment = commentJpaRepository.findById(commentId);
                if (comment.isPresent()) {
                    commentJpaRepository.delete(comment.get());
                    response = true;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        if (!response) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        return response;
    }

    
    private List<HistoryTask> findHistoryTaskByExecutionId(String id) {
        return historyTaskRepository.findByExecutionId(id);
    }

    public CamundaResponse borrarActividad(BorrarActividadRequestDTO request) throws CustomException {

        Optional<Usuario> usuarioOpt = findByNombreUsuario(request.getUsername());
        Boolean respuesta;
        if (usuarioOpt.isEmpty()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
        var respuestasC = new StringBuilder();
        var respuestasI = new StringBuilder();
        try {
            var usuario = usuarioOpt.get();
            var listHistoryTask = findHistoryTaskByExecutionId(request.getIdExecution());
            for (HistoryTask history : listHistoryTask) {
                var requestDTO = new BorrarActividadRequest();
                String comentario = "[AUT_EA] Actividad Eliminada por " +
                        getNombreUsuario(usuario) +
                        " Tarea Eliminada: " +
                        history.getName();

                requestDTO.setCarpetaId(request.getCarpetaId().toString());
                requestDTO.setTaskId(history.getId());
                requestDTO.setUserId(usuario.getId().toString());
                requestDTO.setExecutionId(request.getIdExecution());
                requestDTO.setEstadoEliminacion(ESTADO_ELIMINACION);
                requestDTO.setComentario(comentario);
                respuesta = camundaCoreRepositorio.borrarActividad(requestDTO);

                if (Boolean.TRUE.equals(respuesta)) {
                    respuestasC.append("Eliminada ").append(requestDTO.getComentario()).append("\n");
                } else {
                    respuestasI.append("Error al eliminar ").append(history.getName()).append("\n");

                }
            }

        } catch (Exception e) {
            logger.error("Error en camunda borrar actividad: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, respuestasC.toString(), respuestasI.toString());
    }

    private String getNombreCompletoUsuario(Usuario usuario) {

        String primerN = usuario.getPrimerNombre() == null ? "" : usuario.getPrimerNombre().concat(" ");
        String segundoN = usuario.getSegundoNombre() == null ? "" : usuario.getSegundoNombre().concat(" ");
        String apellidoP = usuario.getApellidoPaterno() == null ? "" : usuario.getApellidoPaterno().concat(" ");
        String apellidoM = usuario.getApellidoMaterno() == null ? "" : usuario.getApellidoMaterno();
        return primerN.concat(segundoN).concat(apellidoP).concat(apellidoM);
    }

    private String getNombreUsuario(Usuario usuario) {

        String primerN = usuario.getPrimerNombre() == null ? "" : usuario.getPrimerNombre().concat(" ");
        String apellidoP = usuario.getApellidoPaterno() == null ? "" : usuario.getApellidoPaterno().concat(" ");
        String apellidoM = usuario.getApellidoMaterno() == null ? "" : usuario.getApellidoMaterno();
        return primerN.concat(apellidoP).concat(apellidoM);
    }

    public String agregarActividad(AgregarActividadRequest request) {
        return camundaCoreRepositorio.agregarActividad(request);
    }

    public CamundaResponse agregarActividad2(AgregarActividad2Request request) throws CustomException {
        if (!StringUtils.hasText(request.getUsername())) {
            throw new CustomException(ErrorCode.BAD_REQUEST);
        }
        var usuario = usuarioRepositorio.findByNombreUsuario(request.getUsername());
        var responsable = usuarioRepositorio.findByNombreUsuario(request.getResponsable());
        if (usuario.isPresent() && responsable.isPresent()) {
            request.setUsuarioId(usuario.get().getId());
            request.setResponsableId(responsable.get().getId());

            StringBuilder comentario = new StringBuilder("[AUT_NA] Inserta nueva actividad en la carpeta ")
                    .append(request.getCarpetaId()).append(" con id_vigente ").append(request.getTaskKey())
                    .append(" y asignado al Usuario ")
                    .append(getNombreUsuario(usuario.get())).append(". Observacion: ")
                    .append(request.getObservacion());
            request.setComentario(comentario.toString());
            String response = camundaCoreRepositorio.agregarActividad2(request);

            if (response.equalsIgnoreCase("Fallido")) {
                throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
            } else {
                return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se agregó correctamente la tarea "
                        .concat(request.getTaskKey())
                        .concat("a la carpeta ").concat(request.getCarpetaId()));
            }
        } else {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
    }

    public String agregarActividadPorProceso(AgregarActividadPorProcesoRequest request) {
        return camundaCoreRepositorio.agregarActividadPorProceso(request);
    }

    public CamundaResponse cambioEstadoCarpeta(CambioEstadoCarpetaDTO cambioEstadoCarpetaDTO) throws CustomException {

        var request = new CambioEstadoCarpetaRequest();

        Optional<Usuario> usuarioOpt = findByNombreUsuario(cambioEstadoCarpetaDTO.getUsername());
        Optional<EstadoCarpeta> estadoCarpetaOpt = Optional.ofNullable(estadoCarpetaRepositorio.findById(cambioEstadoCarpetaDTO.getEstadoNuevoId()).orElse(null));

        if (!(usuarioOpt.isPresent() && estadoCarpetaOpt.isPresent())) {
            throw new CustomException(ErrorCode.ERROR_CAMBIO_ESTADO);
        }

        if (Boolean.TRUE.equals(findIndicadorCarpetaConProceso(cambioEstadoCarpetaDTO.getCarpetaId()))) {
            throw new CustomException(ErrorCode.CARPETA_CON_PROCESOS_ACTIVOS);
        }
        String respuesta;
        try {
            request.setCarpetaId(cambioEstadoCarpetaDTO.getCarpetaId());
            String mensaje = "[AUT_CE] Cambio de Estado realizado por " +
                    getNombreCompletoUsuario(usuarioOpt.get()) + " a " + estadoCarpetaOpt.get().getNombre();
            request.setMensaje(mensaje);
            request.setEstadoNuevo(estadoCarpetaOpt.get());
            request.setUsuarioResponsableId(usuarioOpt.get().getId());
            respuesta = camundaCoreRepositorio.cambioEstadoCarpeta(request);

        } catch (Exception e) {
            logger.error("Error en camunda cambiar estado: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (respuesta.equalsIgnoreCase(RESPUESTA_FALLIDA)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se cambió el estado de la carpeta ".concat(request.getCarpetaId().toString()).concat(SUCCESSFULLY));
    }

    public CamundaResponse descontabilizar(DescontabilizarRequestDTO requestDto) throws CustomException {

        if (Objects.isNull(requestDto.getCarpetaId()) || Objects.isNull(requestDto.getUsername())) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        Optional<Usuario> usuarioOpt = findByNombreUsuario(requestDto.getUsername());

        if (usuarioOpt.isEmpty()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }

        if (!usuarioOpt.get().isSupervisor()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_SUPERVISOR);
        }

        Boolean firmaParticipante = restTemplate.getForEntity(urlParticipantes.concat(FIND_SIGNED_FOLDER).
                concat(requestDto.getCarpetaId().toString()), Boolean.class).getBody();

        if (firmaParticipante != null && !firmaParticipante) {
            throw new CustomException(ErrorCode.UNSIGNED_FOLDER);
        }

        Boolean rolAutDescontaAsignado = restTemplate.getForEntity(urlPerfilamiento.concat(FIND_ASSIGNED_ROLE).
                        concat(RolEnum.COORDINADOR_HIPOTECARIO.get().toString()).concat("/").concat(requestDto.getUsername()),
                Boolean.class).getBody();

        if (rolAutDescontaAsignado != null && !rolAutDescontaAsignado) {
            throw new CustomException(ErrorCode.ASSOCIATE_SUPERVISOR);
        }

        String respuesta;
        try {
            var request = new DescontabilizarRequest();
            request.setNumCarpeta(requestDto.getCarpetaId());

            var proceso = new EtActividadDetalle();
            proceso.setId(EtActividadDetalleEnum.AGREGAR_ACTIVIDAD_DESCONTABILIZACION.get());

            request.setProcesoId(proceso.getId());

            respuesta = camundaCoreRepositorio.descontabilizar(request);
        } catch (Exception e) {
            logger.error("Error en camunda descontabilizar: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (respuesta.equalsIgnoreCase(RESPUESTA_FALLIDA)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Descontabilización exitosa de la carpeta "
                .concat(requestDto.getCarpetaId().toString()));
    }

    public CamundaResponse eliminarActividadHistoria(EliminarActividadHistoriaRequest request) throws CustomException {

        Optional<Usuario> usuarioOpt = findByNombreUsuario(request.getUsuarioName());

        if (usuarioOpt.isEmpty()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
        Boolean respuesta;
        try {
            request.setObservacion(construirMensajeEliminarActividadHist(request.getHistoriasEliminar(), request.getObservacion()));
            request.setUsuarioId(usuarioOpt.get().getId().toString());
            respuesta = camundaCoreRepositorio.eliminarActividadHistoria(request);

        } catch (Exception e) {
            logger.error("Error en camunda eliminar actividad: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (Boolean.FALSE.equals(respuesta)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se eliminaron/actualizaron la/s actividad/es exitosamente");
    }

    private Boolean findIndicadorCarpetaConProceso(Long carpetaId) {

        Boolean tieneProcesoActivo = Boolean.FALSE;
        List<RunningExecution> lista = runningExecutionRepository.findByCarpetaId(carpetaId);
        if (!lista.isEmpty()) {
            tieneProcesoActivo = Boolean.TRUE;
        }
        return tieneProcesoActivo;
    }

    private Optional<Usuario> findByNombreUsuario(String userName) {
        return Optional.ofNullable(usuarioRepositorio.findByNombreUsuario(userName).orElse(null));

    }

    public CamundaResponse rehacerActividad(RehacerActividadRequestDTO rehacerActividadRequestDTO) throws CustomException {

        Optional<Usuario> usuarioOpt = findByNombreUsuario(rehacerActividadRequestDTO.getUsuarioName());
        Optional<Usuario> usuarioResponsableOpt = findByNombreUsuario(rehacerActividadRequestDTO.getUsuarioResponsableName());

        if (!(usuarioOpt.isPresent() && usuarioResponsableOpt.isPresent())) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }

        String respuesta;
        try {
            StringBuilder mensaje = new StringBuilder("[AUT_RHA] Rehacer Actividad realizado por ")
                    .append(getNombreCompletoUsuario(usuarioOpt.get()))
                    .append(" desde ").append(getNombreCompletoUsuario(usuarioOpt.get()))
                    .append(" a ").append(getNombreCompletoUsuario(usuarioOpt.get()));
            var request = new RehacerActividadRequest();
            request.setCarpetaId(rehacerActividadRequestDTO.getCarpetaId());
            request.setComentario(mensaje.toString());
            request.setExecutionId(rehacerActividadRequestDTO.getExecutionId());
            request.setTareaNombreSeleccionada(rehacerActividadRequestDTO.getTareaNombreSeleccionada());
            request.setTareaSeleccionada(rehacerActividadRequestDTO.getTareaSeleccionada());
            request.setTareaSeleccionadaId(rehacerActividadRequestDTO.getTareaSeleccionadaId());
            request.setUsuarioId(usuarioOpt.get().getId());
            request.setUsuarioResponsableId(usuarioResponsableOpt.get().getId());
            respuesta = camundaCoreRepositorio.rehacerActividad(request);

        } catch (Exception e) {
            logger.error("Error en camunda rehacer actividad: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (respuesta.equalsIgnoreCase(RESPUESTA_FALLIDA)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se rehizo la actividad ".concat(rehacerActividadRequestDTO.getCarpetaId().toString()).concat(SUCCESSFULLY));

    }

    public CamundaResponse rehacerActividad2(RehacerActividadRequestDTO2 requestDTO) throws CustomException {
        Optional<Usuario> usuarioOpt = findByNombreUsuario(requestDTO.getUsuarioName());
        Optional<Usuario> usuarioResponsableOpt = findByNombreUsuario(requestDTO.getUsuarioResponsableName());

        if (!(usuarioOpt.isPresent() && usuarioResponsableOpt.isPresent())) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
        String respuesta;

        StringBuilder comentario = new StringBuilder("[AUT_RHA] Rehacer Actividad realizado por ")
                .append(requestDTO.getUsuarioName()).append(" Desde ")
                .append(requestDTO.getActividadActualName()).append(" A ").append(requestDTO.getTareaNombreSeleccionada());
        try {
            var request = new RehacerActividad2Request();
            request.setCustomTaskDefinitionId(requestDTO.getCustomTaskDefinitionId());
            request.setCustomTaskDefinitionProcKey(requestDTO.getCustomTaskDefinitionProcKey());
            request.setCustomTaskDefinitionTaskKey(requestDTO.getCustomTaskDefinitionTaskKey());
            request.setTree(requestDTO.getTree());
            request.setRootProcDefKey(requestDTO.getRootProcDefKey());
            request.setOrden(requestDTO.getOrden());
            request.setEsSatelite(requestDTO.getEsSatelite());
            request.setUsuarioId(usuarioOpt.get().getId());
            request.setResponsableId(usuarioResponsableOpt.get().getId());
            request.setCarpetaId(String.valueOf(requestDTO.getCarpetaId()));
            request.setExcecutionId(requestDTO.getExecutionId());
            request.setComentario(comentario.toString());
            request.setFechaIngreso(new Date());
            respuesta = camundaCoreRepositorio.rehacerActividad2(request);
        } catch (Exception e) {
            logger.error("Error en camunda rehacer actividad: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (respuesta.equalsIgnoreCase(RESPUESTA_FALLIDA)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se rehizo la actividad ".concat(requestDTO.getCarpetaId().toString()).concat(SUCCESSFULLY));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public CamundaResponse reasignarAndComment(ReasignarActividadesRequestDTO reasignarActividadesRequestDTO) throws CustomException {

    	if (reasignarActividadesRequestDTO.getRunningTasks() == null ||
                reasignarActividadesRequestDTO.getRunningTasks().isEmpty()) {
            throw new CustomException(ErrorCode.NO_TASK_PRESENTS);
        }
    	
        Optional<Usuario> usuarioDestinoOpt = findByNombreUsuario(reasignarActividadesRequestDTO.getUsuarioDestino());
        Optional<Usuario> usuarioOrigenOpt = findByNombreUsuario(reasignarActividadesRequestDTO.getUsuarioOrigen());
        Optional<Usuario> usuarioResponsableOpt = findByNombreUsuario(reasignarActividadesRequestDTO.getUsuarioResponsable());
       
        var respuesta = "";
        try {
        	var request = new ReasignarActividadesRequest();
        	if (usuarioDestinoOpt.isPresent() && usuarioOrigenOpt.isPresent() && usuarioResponsableOpt.isPresent()) {
        		request.setUsuarioDestinoId(usuarioDestinoOpt.get().getId());
            	request.setUsuarioOrigenId(usuarioOrigenOpt.get().getId());
            	request.setUsuarioResponsableId(usuarioResponsableOpt.get().getId());
            }else {
                throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
            }
        	request.setTipoReasignacion(reasignarActividadesRequestDTO.getTipoReasignacion());
            request.setFechaTermino(reasignarActividadesRequestDTO.getFechaTermino());
            
            StringBuilder mensaje = new StringBuilder("[AUT_RA] ")
                    .append(getNombreCompletoUsuario(usuarioResponsableOpt.get()))
                    .append(" ha reasignado de ").append(getNombreCompletoUsuario(usuarioOrigenOpt.get()))
                    .append(" a ").append(getNombreCompletoUsuario(usuarioDestinoOpt.get()));
            request.setMensaje(mensaje.toString());
            Integer exitosas = 0;
            Integer fallidas = 0;
        	
            for (RunningTaskRequestDTO runningDTO : reasignarActividadesRequestDTO.getRunningTasks()) {
            	List<RunningTaskRequestDTO> tareasPorAsignar = new ArrayList<>();
            	tareasPorAsignar.add(runningDTO);
            	request.setTareasPorAsignar(tareasPorAsignar);
            	respuesta = camundaCoreRepositorio.reasignarAndComment(request);
            	if (respuesta.equalsIgnoreCase(RESPUESTA_FALLIDA)) {
            		fallidas += 1; 
                }else {
                	exitosas += 1;
                }
            }
            String mensajeRespuesta = "Reasignación Masiva ejecutada. Exitosas: "
            						  .concat(exitosas.toString()).concat(", Fallidas: ").concat(fallidas.toString());
            return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, mensajeRespuesta);
        } catch (Exception e) {
            logger.error("Error en camunda reasignar actividad: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        

    }

    public List<TareasAsignadasResponseDTO> findRunningTaskByUsuario(TareasAsignadasRequestDTO request) throws CustomException {
        try {
            Optional<Usuario> usuario = usuarioRepositorio.findByNombreUsuario(request.getUsername());
            List<RunningTask> tasks = null;
            if (usuario.isPresent()) {
                tasks = runningTaskRepository.findByAssignee(usuario.get().getId());
        		
            } else {
                throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
            }
            List<TareasAsignadasResponseDTO> dtoList = new ArrayList<>();
            for (RunningTask task : tasks) {
            	var dto = new TareasAsignadasResponseDTO();
            	if(Objects.nonNull(task.getRunningExecution()) && Objects.nonNull(task.getRunningExecution().getCarpetaId())) {
            		dto.setNCarpeta(task.getRunningExecution().getCarpetaId());
            	}else {
            		dto.setNCarpeta(0L);
            	}
                dto.setTaskId(task.getId());
                dto.setRut(usuario.get().getRut());
                dto.setNombreCliente(usuario.get().getNombreCompleto());
                dto.setActividadActual(task.getName());
                dto.setFechaInicio(task.getCreateTime());
                dto.setFechaEsperada(task.getDueDate());
                dtoList.add(dto);
            }
            Collections.sort(dtoList, (o1, o2) -> o1.getNCarpeta().compareTo(o2.getNCarpeta()));
            return dtoList;
        } catch (Exception e) {
            logger.error(Constant.ERROR, e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    public CamundaResponse eliminarOperacion(DeleteRequestTo deleteRequestTo) throws CustomException {

        logger.info("Entrada: CamundaCoreService.eliminarOperacion");

        Optional<Carpeta> opCarpeta = Optional.ofNullable(carpetaRepositorio.findById(deleteRequestTo.getCarpetaId()).orElse(null));

        if (opCarpeta.isEmpty()) {
            throw new CustomException(ErrorCode.CARPETA_NOT_FOUND);
        } else {
            if (opCarpeta.get().getEstadoCarpeta() != null && EstadoCarpetaEnum.ELIMINADA.get().equals(opCarpeta.get().getEstadoCarpeta().getId())) {
                throw new CustomException(ErrorCode.CARPETA_ELIMINADA);
            }
            var estadoContabilizacion = getEstadoContabilizacion(opCarpeta.get());
            if (opCarpeta.get().getTipoOperacion() != null && opCarpeta.get().getTipoOperacion().getId().equals(TipoOperacionEnum.FORMALIZACION.get())
                    && estadoContabilizacion != null
                    && estadoContabilizacion.getId().equals(EstadoContabilizacionEnum.ESTADO_5_CONTABILIZADA.get())) {

                throw new CustomException(ErrorCode.OPERACION_YA_CONTABILIZADA);
            }

        }

        var deleteRequestCamundaTo = new DeleteRequestCamundaTo();
        Boolean respuesta;
        Optional<Usuario> usuarioOpt = usuarioRepositorio.findByNombreUsuario(deleteRequestTo.getUsername());
        if (usuarioOpt.isEmpty()) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }
        BeanUtils.copyProperties(deleteRequestTo, deleteRequestCamundaTo);
        deleteRequestCamundaTo.setUsuarioId(usuarioOpt.get().getId());
        try {
            respuesta = camundaCoreRepositorio.eliminarOperacion(deleteRequestCamundaTo);
        } catch (Exception e) {
            logger.error("Error en camunda eliminar operacion: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (Boolean.FALSE.equals(respuesta)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, DELETE_OPERATION);

    }

    public CamundaResponse reasignar(ReasignarRequestDTO reasignarRequestDTO) throws CustomException {

        Optional<Usuario> usuarioOpt = findByNombreUsuario(reasignarRequestDTO.getUsername());
        Optional<Usuario> usuarioDestinoOpt = findByNombreUsuario(reasignarRequestDTO.getNuevoResponsable());

        if (!(usuarioOpt.isPresent() && usuarioDestinoOpt.isPresent())) {
            throw new CustomException(ErrorCode.ERROR_USUARIO_NOT_FOUND);
        }

        Optional<RunningTask> opRunningTask = Optional.ofNullable(runningTaskRepository.findById(reasignarRequestDTO.getIdTask()).orElse(null));
        if (!opRunningTask.isPresent()) {
            throw new CustomException(ErrorCode.RUNNING_TASK_NOT_FOUND);
        }

        String respuesta;
        try {
            StringBuilder mensaje = new StringBuilder("[AUT_RA] ")
                    .append(getNombreCompletoUsuario(usuarioOpt.get()))
                    .append(" ha reasignado de ").append(reasignarRequestDTO.getResponsableActual())
                    .append(" a ").append(getNombreCompletoUsuario(usuarioDestinoOpt.get()));

            var request = new ReasignarRequest();
            request.setIdCarpeta(reasignarRequestDTO.getCarpetaId());
            request.setMensaje(mensaje.toString());
            request.setIdTask(reasignarRequestDTO.getIdTask());
            request.setUserRespId(usuarioDestinoOpt.get().getId());
            respuesta = camundaCoreRepositorio.reasignar(request);

        } catch (Exception e) {
            logger.error("Error en camunda reasignar actividad: {}", e.getMessage());
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        if (respuesta.equalsIgnoreCase(RESPUESTA_FALLIDA)) {
            throw new CustomException(ErrorCode.CAMUNDA_RESPUESTA_FALLIDA);
        }
        return new CamundaResponse(602, CODE_RESPUESTA_EXITOSA, "Se reasignó la actividad exitosamente");
    }

    public EstadoContabilizacion getEstadoContabilizacion(Carpeta carpeta) {

        Optional<Formalizacion> opFormalizacion = Optional.ofNullable(
                formalizacionRepositorio.findByCarpetaId(carpeta.getId()).orElse(null));
        if (opFormalizacion.isPresent()) {
            Optional<FoContabilizacion> opEstadoContabilizacion = Optional.ofNullable(
                    foContabilizacionRepositorio.findByFormalizacionId(opFormalizacion.get().getId()).orElse(null));
            if (opEstadoContabilizacion.isPresent()) {
                return opEstadoContabilizacion.get().getEstadoCont();
            }
        } else {
            return new EstadoContabilizacion();
        }

        return null;
    }

    private String construirMensajeEliminarActividadHist(List<HistoriaCarpetaDTO> listadoEliminarHistoria, String observacion) {

        List<HistoriaCarpetaDTO> historiasEliminar = new ArrayList<>();
        var eliminar = false;
        var restaurar = false;
        var mensajeEliminar = new StringBuilder("Se eliminan actividad(es):");
        var mensajeRestaurar = new StringBuilder("Se Restauran actividad(es):");

        for (HistoriaCarpetaDTO historia : listadoEliminarHistoria) {
            historiasEliminar.add(historia);
            if ("E".equals(historia.getE())) {
                mensajeRestaurar.append(" ").append(historia.getTarea()).append(" (").append(historia.getId())
                        .append("),");
                restaurar = true;
            } else {
                mensajeEliminar.append(" ").append(historia.getTarea()).append(" (").append(historia.getId())
                        .append("),");
                eliminar = true;
            }
        }

        var mensajeMostrar = new StringBuilder();

        if (eliminar && restaurar) {
            mensajeMostrar.append(mensajeEliminar.substring(0, mensajeEliminar.length() - 1)).append(" y ")
                    .append(mensajeRestaurar.substring(0, mensajeRestaurar.length() - 1)).append(OBSERVACION)
                    .append(observacion);
        } else if (restaurar) {
            mensajeMostrar.append(mensajeRestaurar.substring(0, mensajeRestaurar.length() - 1))
                    .append(OBSERVACION).append(observacion);
        } else {
            mensajeMostrar.append(mensajeEliminar.substring(0, mensajeEliminar.length() - 1)).append(OBSERVACION)
                    .append(observacion);
        }

        return "[AUT_EAH] " + mensajeMostrar.toString();

    }

    public List<UsuarioDto> responsables(RequestDTO request) {
        return camundaCoreRepositorio.responsables(request);
    }

    public List<TaskDefResponseTo> findTareasPrevias(TareasPreviasRequestDTO request) {

        List<TaskDefinition> taskDefinitions;
        List<TaskDefResponseTo> taskDefinitionsResponse = new ArrayList<>();

        if (Objects.nonNull(request.getProcDefKey()) && Objects.nonNull(request.getTaskKey())) {

            var actualTask = taskDefinitionRepositorio.findByProcDefKeyAndTaskKey(request.getProcDefKey(), request.getTaskKey());
            taskDefinitions = taskDefinitionRepositorio.findByProcDefKey(request.getProcDefKey());
            for (TaskDefinition taskDefinition : taskDefinitions) {
                if (taskDefinition.getOrden() < actualTask.getOrden()) {
                    
                	setTaskDefinitionResponse(taskDefinitionsResponse, taskDefinition);
                }
            }
        }
        Collections.sort(taskDefinitionsResponse, (o1, o2) -> o1.getOrden().compareTo(o2.getOrden()));
        return taskDefinitionsResponse;
    }

    private void setTaskDefinitionResponse(List<TaskDefResponseTo> taskDefinitionsResponse, TaskDefinition taskDefinition) {
    	
    	var tree = customTaskProcessTreeRepository.findByRootProcDefKeyAndTaskId(
                taskDefinition.getProcDefKey(), taskDefinition.getId());
        if (tree != null) {
            taskDefinition.setTree(tree.getTree());
        }
        var customTaskDef = customTaskDefinitionRepository.findByProcDefKeyAndTaskKey(taskDefinition.getProcDefKey(), taskDefinition.getTaskKey());
        if(customTaskDef.isPresent()){
            taskDefinitionsResponse.add(new TaskDefResponseTo(taskDefinition, customTaskDef.get(), tree));
        }else{
            taskDefinitionsResponse.add(new TaskDefResponseTo(taskDefinition, new CustomTaskDefinition(), tree));
        }
    	
    }
    
    
    
	public List<Usuario> findUsuariosTareasPendientes(RolesOrigenAsigMasivaDTO request) {
		
		List<Long> usuariosCandidatos = usuarioRolRepositorio
				.findUsuariosByInstitucionAndRolId(request.getInstitucionId(), request.getRolId());
		
		List<RunningTaskSearch> tasks = runningTaskSearchRepository.findByAssigneeIn(usuariosCandidatos);
		HashSet<Long> assignees = new HashSet<>();
		for (RunningTaskSearch runningTask : tasks) {
			assignees.add(runningTask.getAssignee());
		}
		List<Long> assigneesFinal = new ArrayList<>(assignees);
		List<Usuario> usuarios = usuarioRepositorio.findByIdIn(assigneesFinal);
		Collections.sort(usuarios, (o1, o2) -> o1.getNombreCompleto().compareTo(o2.getNombreCompleto()));
		return usuarios;
	}

}

package cl.api.activity.service;

import cl.api.activity.error.ErrorCode;
import cl.api.activity.exceptions.CustomException;
import cl.api.activity.models.HistoryTask;
import cl.api.activity.models.HistoryTaskResto;
import cl.api.activity.models.RunningTask;
import cl.api.activity.util.Constant;
import cl.api.activity.util.DividerJsonParameters;
import cl.api.activity.util.UtilJson;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;


@Service
public class TaskService {

    private static final String CAMPO_CARPETA_ID = "carpetaId";
	@PersistenceContext
    private EntityManager entityManager;

    public Map<String, Long> findTasks(String qparams) throws CustomException {
        try{

            HashMap<String, Long> task = new HashMap<>();

            var criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);

            var json = UtilJson.getValuesObject(qparams);
            Map<String, Object> filters = DividerJsonParameters.getFiltersObject(json, Constant.CONST_FILTERS);

            Root<HistoryTaskResto> historyTaskRestoRoot = criteriaQuery.from(HistoryTaskResto.class);
            Root<HistoryTask> historyTaskRoot = criteriaQuery.from(HistoryTask.class);

            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(historyTaskRestoRoot.get("id"), historyTaskRoot.get("id")));

            List<Long> carpetasId = new ArrayList<>();
            List<String> tasksId = new ArrayList<>();

            for (String t : (List<String>) filters.get("taskOperacionIds")) {
                carpetasId.add(Long.valueOf(t.split("-")[0]));
                tasksId.add(t.split("-")[1]);
            }

            var inHistoryTaskResto = criteriaBuilder.in(historyTaskRestoRoot.get(CAMPO_CARPETA_ID));
            inHistoryTaskResto.value(carpetasId);
            predicates.add(inHistoryTaskResto);
            var inHistoryTask = criteriaBuilder.in(historyTaskRoot.get("taskDefKey"));
            inHistoryTask.value(tasksId);
            predicates.add(inHistoryTask);

            criteriaQuery.multiselect(historyTaskRoot.get("taskDefKey").alias("historyTask"), historyTaskRestoRoot.get("id").alias("historyTaskResto"));
            criteriaQuery.where(predicates.toArray(new Predicate[0]));

            List<Object[]> historias = entityManager.createQuery(criteriaQuery).getResultList();

            if (historias.isEmpty()) {
                return null;
            }

            for (Object[] obj : historias) {
                task.put(obj[0].toString(), Long.parseLong(obj[1].toString()));
            }

            return task;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    public Map<Long, Long> lastHistoryRestoInOperacion(String qparams) throws CustomException {
        try{

            HashMap<Long, Long> operacionTarea = new HashMap<>();

            var criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);

            var json = UtilJson.getValuesObject(qparams);
            Map<String, Object> filters = DividerJsonParameters.getFiltersObject(json, Constant.CONST_FILTERS);

            Root<HistoryTaskResto> historyTaskRestoRoot = criteriaQuery.from(HistoryTaskResto.class);

            List<Predicate> predicates = new ArrayList<>();

            List<Long> operacionesids = new ArrayList<>();
            for (Integer id : (List<Integer>) filters.get("operacionesids")) {
                operacionesids.add(Long.valueOf(id.toString()));
            }

            var inHistoryTaskResto = criteriaBuilder.in(historyTaskRestoRoot.get(CAMPO_CARPETA_ID));
            inHistoryTaskResto.value(operacionesids);
            predicates.add(inHistoryTaskResto);

            criteriaQuery.multiselect(historyTaskRestoRoot.get(CAMPO_CARPETA_ID).alias(CAMPO_CARPETA_ID), historyTaskRestoRoot.get("id").alias("historyTaskResto"));
            criteriaQuery.where(predicates.toArray(new Predicate[0]));
            criteriaQuery.groupBy(historyTaskRestoRoot.get(CAMPO_CARPETA_ID), historyTaskRestoRoot.get("id"));
            
            List<Object[]> historias = entityManager.createQuery(criteriaQuery).getResultList();

            if (historias.isEmpty()) {
                return null;
            }

            for (Object[] obj : historias) {
                operacionTarea.put((Long) obj[0], Long.parseLong(obj[1].toString()));
            }

            return operacionTarea;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    public Boolean findTaskByYear(Integer year, Long regionId) throws CustomException {
        try{

            var cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.set(year, 0, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(cal.get(Calendar.YEAR), 0, 1);

            Date fechaInicio =cal.getTime();
            cal.set(cal.get(Calendar.YEAR), 11, 31);
            Date fechaFin = cal.getTime();

            Boolean validate = false;

            var criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Object[]> criteriaQuery = criteriaBuilder.createQuery(Object[].class);

            Root<HistoryTaskResto> historyTaskRestoRoot = criteriaQuery.from(HistoryTaskResto.class);
            Root<RunningTask> runningTaskRoot = criteriaQuery.from(RunningTask.class);

            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(historyTaskRestoRoot.get("carpeta").get("regionId"), regionId));
            var predicate1 = criteriaBuilder.between(runningTaskRoot.get("createTime"), fechaInicio, fechaFin);
            var predicate2 = criteriaBuilder.between(runningTaskRoot.get("dueDate"), fechaInicio, fechaFin);
            predicates.add((criteriaBuilder.or(predicate1, predicate2)));

            criteriaQuery.multiselect(runningTaskRoot.get("id").alias("runningTask"));
            criteriaQuery.where(predicates.toArray(new Predicate[0]));

            List<Object[]> task = entityManager.createQuery(criteriaQuery).getResultList();

            if (task.isEmpty()) {
                validate = true;
            }

            return validate;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }
}

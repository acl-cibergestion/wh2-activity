package cl.api.activity.error;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Error {

    private int index;
    private ErrorCode code;
    private String detail;

    public Error(int index, ErrorCode code, String detail) {
        this.index = index;
        this.code = code;
        this.detail = detail;
    }

}

package cl.api.activity.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ErrorCode {

    //Activity error
    CARPETA_CON_PROCESOS_ACTIVOS(601,"CARPETA_CON_PROCESOS_ACTIVOS", "Para hacer el cambio de estado no pueden existir actividades vigentes.", HttpStatus.BAD_REQUEST),
    ERROR_CAMBIO_ESTADO(601,"ERROR_CAMBIO_ESTADO", "No es posible realizar el cambio de estado, usuario o estado de la carpeta no existen.", HttpStatus.BAD_REQUEST),
    ERROR_USUARIO_NOT_FOUND(601,"ERROR_BORRAR_ACTIVIDAD", "Usuario no encontrado.", HttpStatus.BAD_REQUEST),
    CAMUNDA_RESPUESTA_FALLIDA(601,"CAMUNDA_RESPUESTA_FALLIDA", "No fué posible ejecutar la operación solicitada.", HttpStatus.BAD_REQUEST),

    //DESCONTABILIZAR
    ERROR_USUARIO_NOT_SUPERVISOR(601,"ERROR_USUARIO_NOT_SUPERVISOR", "Imposible Descontabilizar, el usuario no tiene supervisor", HttpStatus.BAD_REQUEST),
    UNSIGNED_FOLDER(601,"UNSIGNED_FOLDER", "Imposible Descontabilizar, la operación no tiene Firmas ingresadas", HttpStatus.BAD_REQUEST),
    ASSOCIATE_SUPERVISOR(601,"NON-ASSOCIATE_SUPERVISOR",  "El Supervisor no está asociado al rol", HttpStatus.BAD_REQUEST),
    //Reasignar
    NO_TASK_PRESENTS(601,"NO_TASK_PRESENTS",  "No existen tareas a reasignar", HttpStatus.BAD_REQUEST),
    RUNNING_TASK_NOT_FOUND(601,"NO_TASK_PRESENTS",  "La tarea a reasignar no existe", HttpStatus.BAD_REQUEST),
    CARPETA_NOT_FOUND(601,"CARPETA_NOT_FOUND",  "La Carpeta no existe", HttpStatus.BAD_REQUEST),
    CARPETA_ELIMINADA(601,"CARPETA_ELIMINADA",  "La Carpeta se encuentra en estado eliminada", HttpStatus.BAD_REQUEST),
    OPERACION_YA_CONTABILIZADA(601,"OPERACION_YA_CONTABILIZADA",  "Operación contabilizada, no se puede eliminar. Debe descontabilizar antes de borrar", HttpStatus.BAD_REQUEST),

    // Internal errors
    INTERNAL_ERROR(500,"INTERNAL_ERROR", "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR),
    BAD_REQUEST(103,"BAD_REQUEST", "The request is invalid or malformed.", HttpStatus.BAD_REQUEST),

    ////Login Service errors 400 - 410
    USER_NOT_FOUND_BY_ID(406, "USER_NOT_FOUND_BY_ID", "Ha ocurrido un error. Por favor comuníquese con el administrador del sistema.",HttpStatus.INTERNAL_SERVER_ERROR),

    //Persona Service Error 480 - 481
    INVALID_PERSONA_ID(480, "INVALID_PERSONA_ID", "No se encuentra una Persona con el Id: ", HttpStatus.BAD_REQUEST),
    PERSONA_NOT_FOUND(404, "PERSONA_NOT_FOUND", "La persona no se encuentra registrada: ", HttpStatus.BAD_REQUEST),
    INVALID_PERSONAJURIDICA_ID(481, "INVALID_PERSONAJURIDICA_ID", "No se encuentra una Persona Juridica con el Id: ", HttpStatus.BAD_REQUEST);
    


    private final int index;
    private final String code;
    private String msg;
    private HttpStatus status;


    ErrorCode(int index, String code, String msg, HttpStatus status) {
        this.index = index;
        this.code = code;
        this.msg = msg;
        this.status = status;
    }
}

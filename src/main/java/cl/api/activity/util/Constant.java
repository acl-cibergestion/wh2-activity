package cl.api.activity.util;

public class Constant {

    private Constant(){

    }
    public static final String SCHEMA = "dbo";

    public static final String URL_CAMUNDA_ENGINE="/core-camunda/app/engine";
    public static final String URL_CAMUNDA_EXPOSED="/core-camunda/app/exposed";
    public static final String CAMUNDA_OPERATION_ADDCOMMENT="/addComment";
    public static final String CAMUNDA_OPERATION_AVANZAR="/avanzar";
    public static final String CAMUNDA_OPERATION_GETPREGUNTA="/getPregunta";
    public static final String CAMUNDA_OPERATION_BORRARACTIVIDAD="/borrarActividad";
    public static final String CAMUNDA_OPERATION_AGREGARACTIVIDAD="/agregarActividad";
    public static final String CAMUNDA_OPERATION_AGREGARACTIVIDAD2="/agregarActividad2";
    public static final String CAMUNDA_OPERATION_AGREGARACTIVIDADPROCESO="/agregarActividadPorProceso";
    public static final String CAMUNDA_OPERATION_CAMBIARESTADO="/cambioEstadoCarpeta";
    public static final String CAMUNDA_OPERATION_DESCONTABILIZAR="/descontabilizar";
    public static final String CAMUNDA_OPERATION_ELIMIANRACTIVIDADHIST="/eliminarActividadHistoria";
    public static final String CAMUNDA_OPERATION_REHACERACTIVIDAD="/rehacerActividad";
    public static final String CAMUNDA_OPERATION_REHACERACTIVIDAD2="/rehacerActividad2";
    public static final String CAMUNDA_OPERATION_REASIGNARACTIVIDAD="/reasignarAndComment";
    public static final String CAMUNDA_OPERATION_ELIMINAR_OPERACION="/eliminarOperacion";
	public static final String CAMUNDA_OPERATION_REASIGNAR = "/reasignar";
	public static final String CAMUNDA_OPERATION_RESPONSABLES = "/getResponsablesByTareaAndInstitucion?idProcDef=";
	public static final String CAMUNDA_OPERATION_RESPONSABLES_TASK = "&taskKey=";
	public static final String CAMUNDA_OPERATION_RESPONSABLES_INST = "&instId=";

    public static final String ERROR="Error :{}";

    public static final String CONST_FILTERS = "filters";

    public static final String ADVANCE="Avanzar";
    public static final String SUCCESSFULLY=" exitosamente";
}

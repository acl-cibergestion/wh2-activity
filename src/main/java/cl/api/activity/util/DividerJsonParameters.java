package cl.api.activity.util;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class DividerJsonParameters {

	private DividerJsonParameters() {
	}

	public static Map<String,String> getFilters(Map<?,?> json, String key){
		var constainsFilters = json.containsKey(key);
		if(constainsFilters) {
			return Optional.ofNullable((Map<String, String>) json.get(key)).orElse(Collections.emptyMap());
		}else {
			return Collections.emptyMap();
		}
    }

	public static Map<String,Object> getFiltersObject(Map<?,?> json, String key){
		var constainsFilters = json.containsKey(key);
		if(constainsFilters) {
			return Optional.ofNullable((Map<String, Object>) json.get(key)).orElse(Collections.emptyMap());
		}else {
			return Collections.emptyMap();
		}
	}

}

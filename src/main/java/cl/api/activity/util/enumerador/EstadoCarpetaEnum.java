package cl.api.activity.util.enumerador;



public enum EstadoCarpetaEnum {

	VIGENTE(Long.valueOf(1)),
    FINALIZADA(Long.valueOf(2)),
    ELIMINADA(Long.valueOf(3));

    /**
     * The id.
     */
    private Long id;

    /**
     * Instantiates a estado carpeta enum.
     *
     * @param id the id
     */
    EstadoCarpetaEnum(Long id) {
        this.id = id;
    }

    /**
     * Gets the.
     *
     * @return the long
     */
    public Long get() {
        return this.id;
    }
}

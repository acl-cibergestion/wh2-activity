package cl.api.activity.util.enumerador;



public enum EstadoContabilizacionEnum {

	SIN_CONTABILIZAR(Long.valueOf(1)),
	RECHAZADA_NO_ENVIADA(Long.valueOf(2)),
	ENVIADA_A_CONTABILIZAR(Long.valueOf(3)),
	RECHAZADA_POR_BANCO(Long.valueOf(4)),
	ESTADO_5_CONTABILIZADA(Long.valueOf(5)),
	ESTADO_6_DESCONTABILIZADA(Long.valueOf(6)),
	ESTADO_7_APROBADA_PARA_CONTABILIZAR(Long.valueOf(7)),
	ESTADO_8_ENVIADA_A_PRECONTABILIZAR(Long.valueOf(8)),
	ESTADO_9_PRECONTABILIZADA(Long.valueOf(9));
	
	
    /**
     * The id.
     */
    private Long id;

    /**
     * Instantiates a estado carpeta enum.
     *
     * @param id the id
     */
    EstadoContabilizacionEnum(Long id) {
        this.id = id;
    }

    /**
     * Gets the.
     *
     * @return the long
     */
    public Long get() {
        return this.id;
    }
}

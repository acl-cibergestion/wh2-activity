package cl.api.activity.util.enumerador;

/**
 * The Enum RolEnum.
 */
public enum RolEnum {

    /** The abogado. */
    ABOGADO(1),
    COORDINADOR_HIPOTECARIO(22);

    /** The codigo. */
    private final Long codigo;

    /**
     * Instantiates a new rol enum.
     *
     * @param id
     *            the id
     */
    RolEnum(final int id) {
	this.codigo = Long.valueOf(id);
    }

    /**
     * Gets the.
     *
     * @return the long
     */
    public Long get() {
	return this.codigo;
    }
}

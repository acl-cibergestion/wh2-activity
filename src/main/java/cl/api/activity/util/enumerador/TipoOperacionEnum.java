package cl.api.activity.util.enumerador;



public enum TipoOperacionEnum {

	FORMALIZACION(Long.valueOf(1)),
    INGRESO_TITULO(Long.valueOf(2));

    /**
     * The id.
     */
    private Long id;

    /**
     * Instantiates a estado carpeta enum.
     *
     * @param id the id
     */
    TipoOperacionEnum(Long id) {
        this.id = id;
    }

    /**
     * Gets the.
     *
     * @return the long
     */
    public Long get() {
        return this.id;
    }
}

package cl.api.activity.util.enumerador;



public enum EtActividadDetalleEnum {

    AGREGAR_ACTIVIDAD_DESCONTABILIZACION(Long.valueOf(1)),
    AGREGAR_ACITVIDAD_AJUSTE_CONDICIONES(Long.valueOf(2)),
    AGREGAR_ACTIVIDAD_CAMBIO_DEUDOR(Long.valueOf(3)),
    AGREGAR_ACITVIDAD_GESTIONAR_ALZAMIENTO(Long.valueOf(4)),
    AGREGAR_ACTIVIDAD_PAGO(Long.valueOf(5)),
    AGREGAR_ACTIVIDAD_CONTABILIZACION(Long.valueOf(6)),
    AGREGAR_ACTIVIDAD_PAGO_FIRMA_CLIENTE(Long.valueOf(7)),
    AGREGAR_ACTIVIDAD_PAGO_MATRICERIA(Long.valueOf(8));

    /**
     * The id.
     */
    private Long id;

    /**
     * Instantiates a new tipo operacion enum.
     *
     * @param id the id
     */
    EtActividadDetalleEnum(Long id) {
        this.id = id;
    }

    /**
     * Gets the.
     *
     * @return the long
     */
    public Long get() {
        return this.id;
    }
}

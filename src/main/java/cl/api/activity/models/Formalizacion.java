package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import cl.api.activity.util.Constant;

import java.io.Serializable;

/**
 * The persistent class for the WFH_FORMALIZACIONES database table.
 */
@Entity
@Table(name = "WFH_FORMALIZACIONES", schema = Constant.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Formalizacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    
    @Column(name = "CARPETA_ID")
    private Long carpetaId;

}

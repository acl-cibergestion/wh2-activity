package cl.api.activity.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ACT_REASIGNACIONES_MASIVAS", schema = "dbo")
@SequenceGenerator(allocationSize = 1, name = "SEC_ID_ACT_REASIG_MASIVAS_GEN", sequenceName = "SEC_ID_ACT_REASIG_MASIVAS")
@Getter
@Setter
@AllArgsConstructor
public class ActReasignacionMasiva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEC_ID_ACT_REASIG_MASIVAS_GEN")
    private Long id;

    @Column(name = "RUNNING_TASK_ID")
    private String runningTask;

    @Column(name = "USUARIO_ORIGEN_ID")
    private Long usuarioOrigenId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_VENCIMIENTO")
    private Date fechaVencimiento;
    
    public ActReasignacionMasiva(String runningTask, Long usuarioOrigenId, Date fechaVencimiento) {
		this.runningTask = runningTask;
		this.usuarioOrigenId = usuarioOrigenId;
		this.fechaVencimiento = fechaVencimiento;
	}
    
}
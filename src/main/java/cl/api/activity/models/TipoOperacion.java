package cl.api.activity.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


import cl.api.activity.util.Constant;

import java.io.Serializable;
import java.math.BigDecimal;


@Entity
@Table(name = "WFH_TIPOS_OPERACION", schema = Constant.SCHEMA)
@Getter
@Setter
public class TipoOperacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private BigDecimal activo;

    private String descripcion;

    private int codigo;

}
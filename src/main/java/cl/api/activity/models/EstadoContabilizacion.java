package cl.api.activity.models;


import cl.api.activity.util.Constant;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "CON_ESTADOS_CONTABILIZACION", schema = Constant.SCHEMA)
@Getter
@Setter
public class EstadoContabilizacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;

}

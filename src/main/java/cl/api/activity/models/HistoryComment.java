package cl.api.activity.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.util.Date;

@Entity
@Table(name = "ACT_HI_COMMENT", schema = "dbo")
@Getter
@Setter
public class HistoryComment {

    @Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "TYPE_")
    private String type;

    @Column(name = "TIME_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;
    
    @OneToOne
    @JoinColumn(name = "USER_ID_", referencedColumnName = "ID" , updatable = false , insertable = false)
    private Usuario usuario;
    
    @Column(name = "USER_ID_")
    private String userId;

    @OneToOne
    @JoinColumn(name = "TASK_ID_", referencedColumnName = "ID_" , updatable = false , insertable = false, nullable = true)
    @NotFound(action = NotFoundAction.IGNORE)
    private HistoryTask historyTask;
    
    @Column(name = "TASK_ID_")
    private String taskId;

    @Column(name = "ROOT_PROC_INST_ID_")
    private String rootProcInstId;

    @OneToOne
    @JoinColumn(name = "PROC_INST_ID_", referencedColumnName = "ID" , updatable = false , insertable = false)
    private Carpeta carpeta;
    
    @Column(name = "PROC_INST_ID_" )
    private String procInstId;

    @Column(name = "ACTION_")
    private String action;

    @Column(name = "MESSAGE_")
    private String message;

    @Column(name = "FULL_MSG_")
    @Lob
    private byte[] fullMessage;

    @Column(name = "TENANT_ID_")
    private String tenantId;

    @Column(name = "REMOVAL_TIME_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date removalTime;
}

package cl.api.activity.models;


import java.io.Serializable;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "ACT_RU_EXECUTION", schema = "dbo")
public class RunningExecution implements Serializable{

    private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "BUSINESS_KEY_")
    private Long carpetaId;

}

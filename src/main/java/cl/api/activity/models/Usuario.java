package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "WFH_USUARIOS", schema = "dbo")
@Getter
@NoArgsConstructor
public class Usuario {

    @Id
    private Long id;

    @Column(name = "PRIMER_NOMBRE")
    private String primerNombre;

    @Column(name = "APELLIDO_PATERNO")
    private String apellidoPaterno;

    @Column(name = "APELLIDO_MATERNO")
    private String apellidoMaterno;

    @Column(name = "NOMBRE_USUARIO")
    private String nombreUsuario;

    @Column(name = "SEGUNDO_NOMBRE")
    private String segundoNombre;

    @Column(name = "RUT")
    private String rut;

    @Column(name = "SUPERVISOR")
    private boolean supervisor;
    
    @Column(name = "INSTITUCION_ID")
    private Long institucion;
    
    public String getNombreCompleto() {
        List<String> list = new ArrayList<>();

        if (this.primerNombre != null && !this.primerNombre.trim().equals("")) {
            list.add(Character.toUpperCase(this.primerNombre.charAt(0)) + this.primerNombre.substring(1).toLowerCase());
        }

        if (this.apellidoPaterno != null && !this.apellidoPaterno.trim().equals("")) {
            list.add(Character.toUpperCase(this.apellidoPaterno.charAt(0))
                    + this.apellidoPaterno.substring(1).toLowerCase());
        }

        if (this.apellidoMaterno != null && !this.apellidoMaterno.trim().equals("")) {
            list.add(Character.toUpperCase(this.apellidoMaterno.charAt(0))
                    + this.apellidoMaterno.substring(1).toLowerCase());
        }

        return StringUtils.join(list, " ");
    }
}

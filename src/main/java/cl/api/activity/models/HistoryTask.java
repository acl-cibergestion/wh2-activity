package cl.api.activity.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ACT_HI_TASKINST", schema = "dbo")
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class HistoryTask implements Serializable{

    private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "NAME_")
    private String name;

    @Column(name = "EXECUTION_ID_")
    private String executionId;

    @Column(name = "TASK_DEF_KEY_")
    private String taskDefKey;

    @Column(name = "DELETE_REASON_")
    private String deleteReason;
    
    @Column(name = "START_TIME_")
    private Date startTime;
    
    @Column(name = "END_TIME_")
    private Date endTime;
    
    @Column(name = "DUE_DATE_")
    private Date dueDate;
    
    @Column(name = "ASSIGNEE_")
    private Long assignee;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private HistoryTaskRestoLight historyTaskResto;

}

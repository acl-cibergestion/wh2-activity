package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "WFH_CARPETAS", schema = "dbo")
@Getter
@NoArgsConstructor
public class Carpeta implements Serializable{

    private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "FLUJO_ID")
    private Flujo flujo;
    
    @OneToOne
    @JoinColumn(name = "ESTADO_ID")
    private EstadoCarpeta estadoCarpeta;
    
    @OneToOne
    @JoinColumn(name = "TIPO_OPERACION_ID")
    private TipoOperacion tipoOperacion;

    @Column(name = "REGION_ID", insertable = false, updatable = false)
    private Long regionId;
}

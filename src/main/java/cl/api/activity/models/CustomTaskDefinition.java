package cl.api.activity.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ACT_CU_TASKDEF", schema = "dbo")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomTaskDefinition {

    @Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "PROC_DEF_KEY_")
    private String procDefKey;

    @Column(name = "TASK_KEY_")
    private String taskKey;

    @Column(name = "CHECKLIST_KEY_")
    private String checklistKey;

    @Column(name = "FORM_KEY_")
    private String formKey;

    @Column(name = "REHACER_DESDE_ESTA_")
    private Boolean rehacerDesdeEsta;

    @Column(name = "REHACER_HACIA_ESTA_")
    private Boolean rehacerHaciaEsta;

    @Column(name = "AVANCE_MASIVO_")
    private Boolean avanceMasivo;

    @Column(name = "TIPO_TAREA_")
    private String tipoTarea;

    @Column(name = "ETAPA")
    private String etapa;

    @Column(name = "ORDEN_")
    private Integer orden;

    @Column(name = "REFERENCIA_ORDEN_")
    private Integer referenciaOrden;

    @Column(name = "ES_SATELITE_")
    private Boolean esSatelite;

    @Column(name = "NAME_")
    private String name;

}

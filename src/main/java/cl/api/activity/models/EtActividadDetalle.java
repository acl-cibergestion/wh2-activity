package cl.api.activity.models;


import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * The persistent class for the WFH_EtActividadDetalleS database table.
 *
 */
@Entity
@Getter
@Setter
@Table(name = "WFH_ET_ACTIVIDAD_DETALLE", schema = "dbo")
public class EtActividadDetalle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String descripcion;

    private String glosa;

    @Column(name="USUARIO_ID")
    private Long usuarioId;
    
}

package cl.api.activity.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACT_CU_TASKPROCESS_TREE", schema = "dbo")
@Getter
@Setter
public class CustomTaskProcessTree implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_")
    private Long id;

    @Column(name = "ROOT_PROCDEF_KEY_")
    private String rootProcDefKey;

    @Column(name = "LEVEL_")
    private Integer level;

    @Column(name = "TREE_")
    private String tree;
    
    @Column(name = "TASK_ID_")
    private String taskId;


}

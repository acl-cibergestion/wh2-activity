package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WFH_FLUJOS", schema = "dbo")
@Getter
@NoArgsConstructor
public class Flujo implements Serializable{

    private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    private String descripcion;

}

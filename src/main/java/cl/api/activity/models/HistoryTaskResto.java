package cl.api.activity.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ACT_HI_TASKINST_RESTO", schema = "dbo")
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class HistoryTaskResto implements Serializable{

    private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "T_ESTANDAR_D")
    private int tEstandarD;

    @Column(name = "T_ESTANDAR_H")
    private int tEstandarH;

    @Column(name = "T_REAL_D")
    private int tRealD;

    @Column(name = "T_REAL_H")
    private int tRealH;

    @Column(name = "DELETE_REASON_OLD")
    private String deleteReasonOld;

    @OneToOne
    @JoinColumn(name = "OPERACION_ID")
    private Carpeta carpeta;

    @Column(name = "OPERACION_ID", insertable = false, updatable = false)
    private Long carpetaId;

    @Column(name = "DETALLE_ETAPA")
    private String detalleEtapa;

    @Column(name = "CONTADOR")
    private int contador;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "WARNING_DUE_DATE_")
    private Date warningDueDate;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private HistoryTask historyTask;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private RunningTask runningTask;

}

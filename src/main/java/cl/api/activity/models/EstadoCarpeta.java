package cl.api.activity.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_ESTADOS_CARPETA database table.
 *
 */
@Getter
@Setter
@Entity
@Table(name = "CON_ESTADOS_CARPETA", schema = "dbo")
public class EstadoCarpeta implements Serializable {

    private static final long serialVersionUID = 7804220083015483057L;

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;

}

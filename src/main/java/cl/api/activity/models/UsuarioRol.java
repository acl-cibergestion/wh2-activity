package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "WFH_US_ROLES", schema = "dbo")
@Getter
@Setter
@NoArgsConstructor
public class UsuarioRol{

    @Id
    @Column(name = "ROL_ID")
    private Long rolId;

    @OneToOne
    @JoinColumn(name = "USUARIO_ID")
    private Usuario usuario;

}

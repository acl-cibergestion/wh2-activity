package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import cl.api.activity.util.Constant;

import java.io.Serializable;


@Entity
@Table(name="WFH_FO_CONTABILIZACIONES", schema = Constant.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class FoContabilizacion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	@Column(name="FORMALIZACION_ID")
	private Long formalizacionId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ESTADO_CONT")
	private EstadoContabilizacion estadoCont;

}
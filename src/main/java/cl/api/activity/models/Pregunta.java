package cl.api.activity.models;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;

import static cl.api.activity.util.Constant.ADVANCE;

@Getter
@Setter
public class Pregunta implements Serializable {

    private static final long serialVersionUID = 1L;
	
    private String preguntaString;
    private String respuesta;
    private HashMap<String, String> opciones;
    private HashMap<String, String> detalleEtapa;

    public Pregunta() {
        this.preguntaString = ADVANCE;
        this.opciones = new HashMap<>();
        this.opciones.put(ADVANCE,ADVANCE);
    }

}

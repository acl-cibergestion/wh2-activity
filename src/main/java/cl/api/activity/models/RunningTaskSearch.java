package cl.api.activity.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "ACT_RU_TASK", schema = "dbo")
public class RunningTaskSearch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_")
    private String id;
    
    @Column(name = "EXECUTION_ID_")
    private String executionId;
    
    @Column(name = "PROC_DEF_ID_")
    private String procDefId;

    @Column(name = "NAME_")
    private String name;
    
    @Column(name = "CREATE_TIME_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "DUE_DATE_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dueDate;
    
    @Column(name = "TASK_DEF_KEY_")
    private String taskDefKey;
    
    @Column(name = "ASSIGNEE_")
    private Long assignee;
    
    @Column(name = "PRIORITY_")
    private Long priority;
    
    

}

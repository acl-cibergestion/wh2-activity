package cl.api.activity.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


import java.io.Serializable;

@Entity
@Table(name = "ACT_CU_TASKDEF", schema = "dbo")
@Getter
@Setter
@NoArgsConstructor
public class TaskDefinition implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_")
    private String id;
    
    @Column(name = "PROC_DEF_KEY_")
    private String procDefKey;
    
    @Column(name = "TASK_KEY_")
    private String taskKey;
    
    @Column(name = "CHECKLIST_KEY_")
    private String checkListKey;
    
    @Column(name = "ETAPA")
    private String etapa;
    
    @Column(name = "ORDEN_")
    private Integer orden;
    
    @Column(name = "NAME_")
    private String name;
    
    @Transient
    private String tree;

}

package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Motivo {

    private Long id;
    private String nombre;
    private boolean activo;

}

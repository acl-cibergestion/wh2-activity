package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CamundaResponse  {

    private Integer index;
	private	String code;
	private	String detail;

	private String commentRespC;
	private String commentRespI;

	public CamundaResponse(Integer index, String code, String detail) {
		this.index = index;
		this.code = code;
		this.detail = detail;
	}

	public CamundaResponse(Integer index, String code, String commentRespC, String commentRespI) {
		this.index = index;
		this.code = code;
		this.commentRespC = commentRespC;
		this.commentRespI = commentRespI;
	}
}

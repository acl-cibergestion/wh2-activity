package cl.api.activity.models.to;



import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DetalleCarpetaDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private Long numCarpeta;
}

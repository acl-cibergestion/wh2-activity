package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DescontabilizarRequestDTO {

    private String username;
	private Long carpetaId;
}

package cl.api.activity.models.to;

import cl.api.activity.models.Carpeta;
import cl.api.activity.models.EtActividadDetalle;
import cl.api.activity.models.Usuario;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgregarActividadPorProcesoRequest  {

    private EtActividadDetalle proceso;
	private Carpeta carpeta;
	private	Usuario responsable;

}

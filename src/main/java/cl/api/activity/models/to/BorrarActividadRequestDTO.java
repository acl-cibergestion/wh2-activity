package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BorrarActividadRequestDTO  {
    
	private String idExecution;
    private Long carpetaId;
	private	String username;
}


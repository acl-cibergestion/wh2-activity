package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RehacerActividadRequest {

    private String tareaNombreSeleccionada;
    private String tareaSeleccionada;
    private Long tareaSeleccionadaId;

    private Long carpetaId;
    private Long usuarioId;
    private String comentario;
    private Long usuarioResponsableId;
	private String executionId;
	
}

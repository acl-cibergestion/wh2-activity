package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteRequestCamundaTo {

    private Long carpetaId;
	private Motivo motivo;
	private String taskId;
	private Long usuarioId;

}

package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReasignarRequestDTO {

	private String nuevoResponsable;
    private String responsableActual;
    private String username;
    private String idTask;
    private Long carpetaId;
		
}

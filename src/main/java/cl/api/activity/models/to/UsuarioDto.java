package cl.api.activity.models.to;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class UsuarioDto {

    private String username;

    private String nombre;

}

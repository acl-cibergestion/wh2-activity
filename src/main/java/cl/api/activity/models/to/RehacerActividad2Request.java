package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RehacerActividad2Request {

    private String customTaskDefinitionId;
    private String customTaskDefinitionProcKey;
    private String customTaskDefinitionTaskKey;

    private String tree;
    private String rootProcDefKey;
    private Integer orden;
    private Boolean esSatelite;

    private String carpetaId;
    private Long usuarioId;
    private Long responsableId;
    private String comentario;
    private Date fechaIngreso;
    private String excecutionId;

}

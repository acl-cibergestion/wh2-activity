package cl.api.activity.models.to;

import cl.api.activity.models.EstadoCarpeta;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CambioEstadoCarpetaRequest  {
	
    private Long carpetaId;
	private EstadoCarpeta estadoNuevo;
	private	String mensaje;
	private	Long usuarioResponsableId;

}

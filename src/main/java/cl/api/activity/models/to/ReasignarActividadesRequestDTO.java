package cl.api.activity.models.to;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReasignarActividadesRequestDTO {

	 private List<RunningTaskRequestDTO> runningTasks;
	 private String usuarioDestino;
	 private String usuarioOrigen;
	 private String tipoReasignacion;
	 private String fechaTermino;
	 private String usuarioResponsable;
	
}

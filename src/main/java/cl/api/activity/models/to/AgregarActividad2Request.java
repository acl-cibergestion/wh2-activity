package cl.api.activity.models.to;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgregarActividad2Request  {

	private String username;
	private String carpetaId;
	private String tree;
	private String responsable;
	private Long responsableId;
	private String taskKey;
	private Long usuarioId;
	private String comentario;
	private String fechaIngreso;
	private String observacion;

}

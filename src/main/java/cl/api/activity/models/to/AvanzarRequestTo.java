package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AvanzarRequestTo {

    private List<String> idExecution;
	private String preguntaRespuesta;
	private String username;
}

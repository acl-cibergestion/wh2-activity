package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RehacerActividadRequestDTO {

    private String tareaNombreSeleccionada;
    private String tareaSeleccionada;
    private Long tareaSeleccionadaId;
    private Long carpetaId;
    private String usuarioName;
    private String comentario;
    private String usuarioResponsableName;
	private String executionId;
	
}

package cl.api.activity.models.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TareasAsignadasResponseDTO {

    private Long nCarpeta;
    private String rut;
    private String nombreCliente;
    private String actividadActual;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    private Date fechaInicio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    private Date fechaEsperada;
    private String taskId;

}

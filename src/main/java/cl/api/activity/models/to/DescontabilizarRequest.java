package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DescontabilizarRequest  {

    private Long procesoId;
	private Long numCarpeta;

}

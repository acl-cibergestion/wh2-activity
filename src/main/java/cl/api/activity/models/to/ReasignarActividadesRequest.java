package cl.api.activity.models.to;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReasignarActividadesRequest {

	 private List<RunningTaskRequestDTO> tareasPorAsignar;
	 private Long usuarioDestinoId;
	 private Long usuarioOrigenId;
	 private String tipoReasignacion;
	 private String fechaTermino;
	 private Long usuarioResponsableId;
	 private String mensaje;
}

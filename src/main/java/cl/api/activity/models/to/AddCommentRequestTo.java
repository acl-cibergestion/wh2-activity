package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddCommentRequestTo {

    private String comment;
	private String taskId;
	private String carpetaId;
	private String username;
	private String usuarioId;
	private String idOperacion;

}

package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TareasPreviasRequestDTO {
	
	private String taskKey;
	private String procDefKey;

}

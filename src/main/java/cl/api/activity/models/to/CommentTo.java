package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentTo {

    private String id;
    private String task;
    private String message;
    private String fullMessage;
    private String user;
    private String username;
    private String time;
    private String carpeta;

}

package cl.api.activity.models.to;


import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoriaCarpetaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String num;

    private String tarea;

    private String taskDefKey;

    private String e;

    private String fechaInicio;

    private String fechaEsperada;

    private String fechaTermino;

    private String responsable;

    private String estandarD;

    private String estandarH;

    private String realD;

    private String realH;

    private String description;

    private String operacion;

    private String flujo;

    /* INDICADORES */
    private boolean indActivo;

    private int dentroEstandar;

    private boolean selectedDelete;

    private Long tipoOperacion;

    
}

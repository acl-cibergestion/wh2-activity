package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDTO {
	
	private String idProcDef;
	private String taskKey;
	private Long instId;

}

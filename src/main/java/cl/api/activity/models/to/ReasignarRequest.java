package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReasignarRequest {

	 private Long userRespId;
	 private String idTask;
	 private Long idCarpeta;
	 private String mensaje;

}

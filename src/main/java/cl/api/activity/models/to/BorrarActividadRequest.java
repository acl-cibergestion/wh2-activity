package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BorrarActividadRequest  {
	private String carpetaId;
	private String taskId;
	private String userId;
	private String executionId;
	private String estadoEliminacion;
	private String comentario;
	
}


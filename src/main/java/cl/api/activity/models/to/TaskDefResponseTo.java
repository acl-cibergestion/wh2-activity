package cl.api.activity.models.to;

import cl.api.activity.models.CustomTaskDefinition;
import cl.api.activity.models.CustomTaskProcessTree;
import cl.api.activity.models.TaskDefinition;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskDefResponseTo {

    private String id;
    private String procDefKey;
    private String taskKey;
    private String checkListKey;
    private String etapa;
    private Integer orden;
    private String name;
    private String tree;
    private String rootProcDefKey;
    private boolean esSatelite;

    public TaskDefResponseTo(TaskDefinition taskDefinition, CustomTaskDefinition customTaskDefinition, CustomTaskProcessTree customTaskProcessTree) {
        this.id = taskDefinition.getId();
        this.procDefKey = taskDefinition.getProcDefKey();
        this.taskKey = taskDefinition.getTaskKey();
        this.checkListKey = taskDefinition.getCheckListKey();
        this.etapa = taskDefinition.getEtapa();
        this.orden = taskDefinition.getOrden();
        this.name = taskDefinition.getName();
        this.tree = taskDefinition.getTree();
        this.esSatelite = customTaskDefinition.getEsSatelite();
        if(customTaskProcessTree != null){
            this.rootProcDefKey = customTaskProcessTree.getRootProcDefKey();
        }
    }
}

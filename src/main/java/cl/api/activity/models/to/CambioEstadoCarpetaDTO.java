package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CambioEstadoCarpetaDTO {
	
	private Long carpetaId;
	private String username;
	private Long estadoNuevoId;
	
}

package cl.api.activity.models.to;

public interface ExcecutionDTOI {
	
	String getIdTask();
    String getIdExecution();
    Long getCarpetaId();
}

package cl.api.activity.models.to;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EliminarActividadHistoriaRequest  {

    private List<HistoriaCarpetaDTO> historiasEliminar;
	private String observacion;
	private	String excecutionId;
	private	String usuarioId;
	private String usuarioName;
	
}

package cl.api.activity.models.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ComienzaFlujoRequest {

    private String flowKey;

    private Long carpetaId;

}

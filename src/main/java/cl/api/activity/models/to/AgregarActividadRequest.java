package cl.api.activity.models.to;

import java.util.*;

import cl.api.activity.models.Usuario;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgregarActividadRequest  {
    private String tarea;
	private String carpetaId;
	private String tareaNombre;
	private	String usuarioId;
	private String comentario;
	private Usuario responsable;
	private Date fechaIngreso;
}

package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RolesOrigenAsigMasivaDTO {
	
	private Long institucionId;
	private Long rolId;

}

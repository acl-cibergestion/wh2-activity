package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteRequestTo {

    private Long carpetaId;
	private Motivo motivo;
	private String taskId;
	private String username;

}

package cl.api.activity.models.to;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RehacerActividadRequestDTO2 {

    private String customTaskDefinitionId;
    private String customTaskDefinitionProcKey;
    private String customTaskDefinitionTaskKey;
    private String tree;
    private String rootProcDefKey;

    private Integer orden;
    private Boolean esSatelite;

    private String tareaNombreSeleccionada;
    private Long carpetaId;
    private String usuarioName;
    private String usuarioResponsableName;
	private String executionId;
    private String actividadActualName;
	
}

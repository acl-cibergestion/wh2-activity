package cl.api.activity.models.to;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExcecutionDTO implements ExcecutionDTOI{
	
	String idTask;
    String idExecution;
    Long carpetaId;
    
	@Override
	public String getIdTask() {
		return this.idTask;
	}
	@Override
	public String getIdExecution() {
		return this.getIdExecution();
	}
	@Override
	public Long getCarpetaId() {
		return this.getCarpetaId();
	}
}

package cl.api.activity.repositories;

import cl.api.activity.models.RunningExecution;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RunningExecutionRepository extends JpaRepository<RunningExecution, String> {

	List<RunningExecution> findByCarpetaId(Long carpetaId);
}

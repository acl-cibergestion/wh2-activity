package cl.api.activity.repositories;

import cl.api.activity.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {
    
	Optional<Usuario> findByNombreUsuario(String nombreUsuario);

	List<Usuario> findByIdIn(List<Long> assigneesFinal);
}

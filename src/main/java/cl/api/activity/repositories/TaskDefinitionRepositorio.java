package cl.api.activity.repositories;

import cl.api.activity.models.TaskDefinition;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskDefinitionRepositorio extends JpaRepository<TaskDefinition, String> {

	TaskDefinition findByProcDefKeyAndTaskKey(String procDefKey, String taskKey);

	List<TaskDefinition> findByProcDefKey(String procDefKey);
}

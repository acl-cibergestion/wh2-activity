package cl.api.activity.repositories;

import cl.api.activity.models.HistoryTaskResto;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryTaskRestoRepository extends JpaRepository<HistoryTaskResto, String> {
    
	List<HistoryTaskResto> findAll(Specification<HistoryTaskResto> specification);
}

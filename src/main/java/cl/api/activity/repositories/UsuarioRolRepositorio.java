package cl.api.activity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.api.activity.models.UsuarioRol;


@Repository
public interface UsuarioRolRepositorio extends JpaRepository<UsuarioRol, Long>{

	@Query("select u.id from UsuarioRol ur join ur.usuario u where u.institucion=?1 and ur.rolId=?2 ")
	List<Long> findUsuariosByInstitucionAndRolId(Long institucionId, Long rolId);

	@Query("select u.id from UsuarioRol ur join ur.usuario u where u.institucion=?1")
	List<Long> findUsuariosByInstitucion(Long institucionId);

    
}

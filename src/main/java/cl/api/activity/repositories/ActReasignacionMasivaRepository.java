package cl.api.activity.repositories;

import cl.api.activity.models.ActReasignacionMasiva;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ActReasignacionMasivaRepository extends JpaRepository<ActReasignacionMasiva, Long> {
   
}

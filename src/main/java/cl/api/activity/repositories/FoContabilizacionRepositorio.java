package cl.api.activity.repositories;

import cl.api.activity.models.FoContabilizacion;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoContabilizacionRepositorio extends JpaRepository<FoContabilizacion, Long> {

	Optional<FoContabilizacion> findByFormalizacionId(Long id);

}

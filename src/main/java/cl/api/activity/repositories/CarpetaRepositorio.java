package cl.api.activity.repositories;

import cl.api.activity.models.Carpeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarpetaRepositorio extends JpaRepository<Carpeta, Long> {
}

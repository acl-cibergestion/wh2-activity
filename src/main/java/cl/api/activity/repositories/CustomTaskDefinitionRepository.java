package cl.api.activity.repositories;

import cl.api.activity.models.CustomTaskDefinition;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomTaskDefinitionRepository extends JpaRepository<CustomTaskDefinition, Long> {

    Optional<CustomTaskDefinition> findByProcDefKeyAndTaskKey(String string, String taskDefKey);

}

package cl.api.activity.repositories;

import cl.api.activity.models.CustomTaskProcessTree;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomTaskProcessTreeRepository extends JpaRepository<CustomTaskProcessTree, String> {

	CustomTaskProcessTree findByRootProcDefKeyAndTaskId(String procDefKey, String id);
}

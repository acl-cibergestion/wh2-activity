package cl.api.activity.repositories;

import cl.api.activity.models.HistoryComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentJpaRepository extends JpaRepository<HistoryComment, String> {

    List<HistoryComment> findByProcInstIdOrderByIdDesc(String carpetaId);
}

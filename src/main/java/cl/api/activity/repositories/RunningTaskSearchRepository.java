package cl.api.activity.repositories;

import cl.api.activity.models.RunningTaskSearch;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RunningTaskSearchRepository extends JpaRepository<RunningTaskSearch, String> {
    
	List<RunningTaskSearch> findByAssigneeIn(List<Long> usuariosCandidatos);

}

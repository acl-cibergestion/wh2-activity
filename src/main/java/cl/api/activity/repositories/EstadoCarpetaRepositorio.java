package cl.api.activity.repositories;

import cl.api.activity.models.EstadoCarpeta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoCarpetaRepositorio extends JpaRepository<EstadoCarpeta, Long> {
}

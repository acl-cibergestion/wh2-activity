package cl.api.activity.repositories;

import cl.api.activity.models.Formalizacion;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormalizacionRepositorio extends JpaRepository<Formalizacion, Long> {

	Optional<Formalizacion> findByCarpetaId(Long id);
}

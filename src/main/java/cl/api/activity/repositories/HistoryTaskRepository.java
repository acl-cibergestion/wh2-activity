package cl.api.activity.repositories;

import cl.api.activity.models.HistoryTask;
import cl.api.activity.models.HistoryTaskResto;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HistoryTaskRepository extends JpaRepository<HistoryTask, String> {
    
	List<HistoryTask> findByExecutionId(String id);

	Optional<HistoryTask> findById(String id);

	List<HistoryTask> findAll(Specification<HistoryTaskResto> specification);

	List<HistoryTask> findByEndTimeIsNullAndAssigneeIn(List<Long> usuariosCandidatos);

	List<HistoryTask> findByEndTimeIsNullAndAssignee(Long id);
}

package cl.api.activity.repositories.impl;

import cl.api.activity.models.to.*;
import cl.api.activity.util.Constant;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Repository;

import org.springframework.web.client.RestTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.api.activity.models.*;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CamundaCoreRepositorio {

    private final Logger logger = LoggerFactory.getLogger(CamundaCoreRepositorio.class);
    private static final String LOG_MESSAGE_INVOCANDO = "invocando: {}";
    private static final String LOG_MESSAGE_ERROR = "Error CamundaCoreRepositorio: {}";

    @Value("${camunda.url}")
    private String camundaUrl;

    public boolean addComment(AddCommentRequestTo commentRequest) {

        logger.info("Entrada: CamundaCoreRepositorio.addComment");
        var response = false;

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_ADDCOMMENT;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            response = restTemplate.postForObject(uri, commentRequest, Boolean.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return response;

    }


    public String avanzar(String idExecution, String preguntaRespuesta, Long usuarioEjecutorId) {

        logger.info("Entrada: CamundaCoreRepositorio.avanzar");
        var response = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_ENGINE + Constant.CAMUNDA_OPERATION_AVANZAR;
            var parametros = "?";
            parametros += "idExecution=" + idExecution + "&";
            parametros += "preguntaRespuesta=" + preguntaRespuesta + "&";
            parametros += "usuarioEjecutorId=" + usuarioEjecutorId;

            String uriInvoke = uri + parametros;

            logger.info(LOG_MESSAGE_INVOCANDO, uriInvoke);
            var restTemplate = new RestTemplate();
            response = restTemplate.getForObject(uriInvoke, String.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return response;

    }


    public Pregunta getPregunta(String idExecution) {

        logger.info("Entrada: CamundaCoreRepositorio.getPregunta");
        var response = new Pregunta();

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_ENGINE + Constant.CAMUNDA_OPERATION_GETPREGUNTA;
            var parametros = "?";
            parametros += "idExecution=" + idExecution;

            String uriInvoke = uri + parametros;

            logger.info(LOG_MESSAGE_INVOCANDO, uriInvoke);
            var restTemplate = new RestTemplate();
            response = restTemplate.getForObject(uriInvoke, Pregunta.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return response;

    }


    public Boolean borrarActividad(BorrarActividadRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.borrarActividad");
        var actividadBorrada = Boolean.FALSE;

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_BORRARACTIVIDAD;

            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            actividadBorrada = restTemplate.postForObject(uri, request, Boolean.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return actividadBorrada;
    }


    public String agregarActividad(AgregarActividadRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.agregarActividad");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_AGREGARACTIVIDAD;

            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }


    public String agregarActividad2(AgregarActividad2Request request) {

        logger.info("Entrada: CamundaCoreRepositorio.agregarActividad2");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_AGREGARACTIVIDAD2;

            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }


    public String agregarActividadPorProceso(AgregarActividadPorProcesoRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.agregarActividadPorProceso");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_AGREGARACTIVIDADPROCESO;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }


    public String cambioEstadoCarpeta(CambioEstadoCarpetaRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.cambioEstadoCarpeta");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_CAMBIARESTADO;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }
        return respuesta;
    }


    public String descontabilizar(DescontabilizarRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.descontabilizar");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_DESCONTABILIZAR;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }


    public boolean eliminarActividadHistoria(EliminarActividadHistoriaRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.eliminarActividadHistoria");
        var actividadEliminada = Boolean.FALSE;

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_ELIMIANRACTIVIDADHIST;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            actividadEliminada = restTemplate.postForObject(uri, request, Boolean.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return actividadEliminada;
    }


    public String rehacerActividad(RehacerActividadRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.rehacerActividad");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_REHACERACTIVIDAD;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }

    public String rehacerActividad2(RehacerActividad2Request request) {

        logger.info("Entrada: CamundaCoreRepositorio.rehacerActividad2");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_REHACERACTIVIDAD2;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }


    public String reasignarAndComment(ReasignarActividadesRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.reasignarAndComment");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_REASIGNARACTIVIDAD;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {

            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }

    public Boolean eliminarOperacion(DeleteRequestCamundaTo request) {

        logger.info("Entrada: CamundaCoreRepositorio.eliminarOperacion");
        var operacionBorrada = Boolean.FALSE;

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_ELIMINAR_OPERACION;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            operacionBorrada = restTemplate.postForObject(uri, request, Boolean.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return operacionBorrada;
    }


    public String reasignar(ReasignarRequest request) {

        logger.info("Entrada: CamundaCoreRepositorio.reasignar");
        var respuesta = "";

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_EXPOSED + Constant.CAMUNDA_OPERATION_REASIGNAR;
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.postForObject(uri, request, String.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }

    public List<UsuarioDto> responsables(RequestDTO request) {

        logger.info("Entrada: CamundaCoreRepositorio.responsables");
        List<UsuarioDto> respuesta = new ArrayList<>();

        try {
            String uri = camundaUrl + Constant.URL_CAMUNDA_ENGINE + Constant.CAMUNDA_OPERATION_RESPONSABLES + request.getIdProcDef() +
                    Constant.CAMUNDA_OPERATION_RESPONSABLES_TASK + request.getTaskKey() + Constant.CAMUNDA_OPERATION_RESPONSABLES_INST +
                    request.getInstId();
            logger.info(LOG_MESSAGE_INVOCANDO, uri);
            var restTemplate = new RestTemplate();
            respuesta = restTemplate.getForObject(uri, List.class);

        } catch (Exception e) {
            logger.error(LOG_MESSAGE_ERROR, e.getMessage());
        }

        return respuesta;
    }

}

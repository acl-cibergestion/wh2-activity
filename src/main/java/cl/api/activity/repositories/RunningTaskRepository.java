package cl.api.activity.repositories;

import cl.api.activity.models.RunningTask;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RunningTaskRepository extends JpaRepository<RunningTask, String> {
    
	List<RunningTask> findByAssignee(Long id);

}
